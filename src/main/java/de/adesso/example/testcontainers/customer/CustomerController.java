package de.adesso.example.testcontainers.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;


@RestController
public class CustomerController {

    @Autowired
    CustomerRepository repository;

    @RequestMapping("/save")
    public String process(){
        // save a single Customer
        repository.save(new Customer("Jack", "Smith"));

        // save a list of Customers
        repository.saveAll(Arrays.asList(new Customer("Adam", "Johnson"), new Customer("Kim", "Smith"),
                new Customer("David", "Williams"), new Customer("Peter", "Davis")));

        return "Done";
    }


    @RequestMapping(value = "/findall", produces = "application/json")
    public ResponseEntity<Iterable<Customer>> findAll() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/findbyid", produces = "application/json")
    public ResponseEntity<Customer> findById(@RequestParam("id") long id) {
        return ResponseEntity.of(repository.findById(id));
    }

    @RequestMapping(value = "/findbylastname", produces = "application/json")
    public ResponseEntity<Iterable<Customer>> fetchDataByLastName(@RequestParam("lastname") String lastName) {
        return new ResponseEntity<>(repository.findByLastName(lastName), HttpStatus.OK);
    }
}
