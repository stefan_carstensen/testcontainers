package de.adesso.example.testcontainers.customer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CustomerRepository customerRepository;


    @Test
    public void findByLastName_thenReturnCustomer() {
        // given
        Customer alex = new Customer("Alex", "Cornelisen");
        entityManager.persist(alex);
        entityManager.flush();

        // when
        List<Customer> found = customerRepository.findByLastName("Cornelisen");

        // then
        assertThat(found.size(), is(1));
        Customer customer = found.get(0);
        assertThat(customer.getId(), notNullValue());
        assertThat(customer.getFirstName(), is("Alex"));
        assertThat(customer.getLastName(), is("Cornelisen"));

    }

}
